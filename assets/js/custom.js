function loadMainContent(url) {
	$.post(site_url + url, {}, function (result) {
		$('#main_content').html(result);
		$('#content_now').val(url);
	}, "html");
}
function main_content(obj){
	$("#content_list").hide();
	$("#content_input").hide();
	$("#content_detail").hide();
	$("#" + obj).show();
}
function load_list(url,pg){
	$.post(site_url + url + '/' + pg, {}, function (result) {
		main_content('content_list');
		$('#list_result').html(result);
	}, "html");
}
function load_input(url,id){
    $.post(site_url + url + '/'+ id, {}, function (result) {
		main_content('content_input');
		$('#content_input').html(result);
	}, "html");
}
function handle_delete(url,id){
    $.post(site_url + url + '/'+ id, {}, function (result) {
		main_content('content_list');
		$('#list_result').html(result);
	}, "html");
}