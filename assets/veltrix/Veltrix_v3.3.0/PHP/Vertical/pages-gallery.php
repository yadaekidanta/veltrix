<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Gallery | Veltrix - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/images/favicon.ico">

    <!-- Lightbox css -->
    <link href="public/libs/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
    <?php include 'layouts/headerStyle.php'; ?>

</head>

<?php include 'layouts/master.php'; echo setLayout();?>

    <!-- Begin page -->
    <div id="layout-wrapper">

        <?php include 'layouts/topbar.php'; ?>
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="font-size-18">Gallery</h4>
                                <ol class="breadcrumb mb-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Veltrix</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Extra Pages</a></li>
                                    <li class="breadcrumb-item active">Gallery</li>
                                </ol>
                            </div>
                        </div>


                        <?php include 'layouts/settingButton.php'; ?>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-1.jpg" class="gallery-popup" title="Open Imagination">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-1.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Open Imagination</h4>
                                            <p>
                                                <img src="public/images/users/user-1.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-2.jpg" class="gallery-popup" title="Locked Steel Gate">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-2.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Locked Steel Gate</h4>
                                            <p>
                                                <img src="public/images/users/user-2.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-3.jpg" class="gallery-popup" title="Mac Sunglasses">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-3.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Mac Sunglasses</h4>
                                            <p>
                                                <img src="public/images/users/user-3.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-4.jpg" class="gallery-popup" title="Morning Dew">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-4.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Morning Dew</h4>
                                            <p>
                                                <img src="public/images/users/user-4.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-5.jpg" class="gallery-popup" title="Console Activity">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-5.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Console Activity</h4>
                                            <p>
                                                <img src="public/images/users/user-5.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-6.jpg" class="gallery-popup" title="Shake It!">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-6.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Shake It!</h4>
                                            <p>
                                                <img src="public/images/users/user-6.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-7.jpg" class="gallery-popup" title="Backpack Content">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-7.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Backpack Content</h4>
                                            <p>
                                                <img src="public/images/users/user-1.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-8.jpg" class="gallery-popup" title="Sunset Bulb Glow">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-8.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Sunset Bulb Glow</h4>
                                            <p>
                                                <img src="public/images/users/user-2.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-9.jpg" class="gallery-popup" title="Open Imagination">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-9.jpg" alt="img" class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Open Imagination</h4>
                                            <p>
                                                <img src="public/images/users/user-3.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-10.jpg" class="gallery-popup" title="Console Activity">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-10.jpg" alt="img"
                                            class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Console Activity</h4>
                                            <p>
                                                <img src="public/images/users/user-4.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-11.jpg" class="gallery-popup" title="Open Imagination">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-11.jpg" alt="img"
                                            class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Open Imagination</h4>
                                            <p>
                                                <img src="public/images/users/user-5.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <a href="public/images/gallery/work-12.jpg" class="gallery-popup" title="Shake It!">
                                <div class="project-item">
                                    <div class="overlay-container">
                                        <img src="public/images/gallery/work-12.jpg" alt="img"
                                            class="gallery-thumb-img">
                                        <div class="project-item-overlay">
                                            <h4>Shake It!</h4>
                                            <p>
                                                <img src="public/images/users/user-6.jpg" alt="user"
                                                    class="avatar-sm rounded-circle" />
                                                <span class="ml-2">Curtis Marion</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->



                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->

            <?php include 'layouts/footer.php'; ?>

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <?php include 'layouts/rightbar.php'; ?>

    <?php include 'layouts/footerScript.php'; ?>

    <!-- Magnific Popup-->
    <script src="public/libs/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Tour init js-->
    <script src="public/js/pages/gallery.init.js"></script>

    <?php include "layouts/content-end.php"; ?>