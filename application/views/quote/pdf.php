<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>Contract #<?= $contract['contract_number'] ?></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css	" type="text/css" rel="stylesheet" />
</head>
<style media="screen">
  .table-border tr>th{
    border:1px solid black;
    padding: 5px;
    font-size: 12px;
  }
  .table-border tr>td{
    border:1px solid black;
    padding: 5px;
    font-size: 12px;
  }
</style>
<body>
  <table style="width:100%;">
    <tr>
      <td style="text-align:right;"> <b> Contract #<?= $contract['contract_number'] ?> </b> </td>
    </tr>
    <tr>
      <td style="text-align:right;"> Event Date : <?= ($contract['event_date']) ?> </td>
    </tr>

  </table>

  <table style="width:100%;margin-top: 20px;">
    <td>
      <table style="width:50%;">
        <tr>
          <td><b> User 1 :</b></td>
        </tr>
        <tr>
          <td>Name : <?= $user1['name'] ?></td>
        </tr>
        <tr>
          <td>Phone Number : <?= $user1['cell_number'] ?></td>
        </tr>
        <tr>
          <td>Email : <?= $user1['email'] ?></td>
        </tr>
      </table>
    </td>
    <td>
      <table style="width:50%;">
        <tr>
          <td><b> User 2 :</b></td>
        </tr>
        <tr>
          <td>Name : <?= $user2['name'] ?></td>
        </tr>
        <tr>
          <td>Phone Number : <?= $user2['cell_number'] ?></td>
        </tr>
        <tr>
          <td>Email : <?= $user2['email'] ?></td>
        </tr>
      </table>
    </td>
  </table>
  <table style="width:100%;margin-top: 20px;" class="table-border">
    <thead>
      <tr>
        <th style="width:3%;">No</th>
        <th>Nama Package</th>
        <th>Group</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
    <?php
      $total=0;
      $no=0;
      foreach ($contract_detail->result_array() as $row):
        $no++;
        $total = $total+$row['harga'];
    ?>
        <tr>
          <td style="text-align:center;"><?=$no ?></td>
          <td><?= $row['nama_tarif'] ?></td>
          <td><?= $row['group'] ?></td>
          <td style="text-align:right;"><?= number_format($row['harga']) ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="3" style="text-align:right;"> <b>Total : </b></td>
        <td style="text-align:right;"><b><?= number_format($total) ?></b></td>
      </tr>
    </tfoot>
  </table>
  <h4 style="margin-top: 20px;">Payment Plans : </h4>
  <table style="width:100%;" class="table-border">
    <thead>
      <tr>
        <th style="width:3%;">No</th>
        <th>Amount</th>
        <th>Due Date</th>
        <th>Description</th>
        <th>Payment sequence</th>
      </tr>
    </thead>
    <tbody>
    <?php
      $total=0;
      $no=0;
      foreach ($contracts_payment->result_array() as $row):
        $no++;
    ?>
        <tr>
          <td style="text-align:center;"><?=$no ?></td>
          <td style="text-align:right;"><?= number_format($row['amount']) ?></td>
          <td><?= $row['due_date'] ?></td>
          <td><?= $row['desc'] ?></td>
          <td><?= $row['payment_sequence'] ?></td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</body>
</html>
