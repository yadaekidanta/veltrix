<table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    <thead>
    <tr>
        <th>No</th>
        <th>Contract Number</th>
        <th>Event Date</th>
        <th>Name 1</th>
        <th>Name 2</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?
    $no = ($paging['current'] - 1) * $paging['limit'];
    foreach ($list->result_array() as $row) {
        $no++;
    ?>
        <tr>
            <td><?=$no;?></td>
            <td><?=$row['contract_number'];?></td>
            <td><?=$row['event_date'];?></td>
            <td><?=$row['nama1'];?></td>
            <td><?=$row['nama2'];?></td>
            <td>
                <a>Edit</a>
                <a>Hapus</a>
            </td>
        </tr>
    <?
    }
    ?>
    </tbody>
</table>
<?= $paging['list'] ?>