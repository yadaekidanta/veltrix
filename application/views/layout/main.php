<!doctype html>
<html lang="en">
    <?php $this->load->view('layout/head'); ?>
    <body data-sidebar="dark">
        <div id="layout-wrapper">
            <?php $this->load->view('layout/header'); ?>
            <?php $this->load->view('layout/left_sidebar'); ?>
            <div class="main-content">
                <div class="page-content">
                    <div id="main_content"></div>
                </div>
                <?php $this->load->view('layout/footer'); ?>
            </div>
        </div>
        <?php $this->load->view('layout/right_sidebar'); ?>
        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
        <!-- JAVASCRIPT -->
        <?php $this->load->view('layout/js'); ?>
    </body>
</html>