<?php
function gen_paging($page_data=array(), $hide='')
{
    // $language = get_language();
    // if($language == "id"){
    $teks = "Menampilkan";
    $teks_1 = "hingga";
    $teks_2 = "dari";
    $teks_3 = "catatan";
    // }elseif($language == "en"){
    // 	$teks = "Displaying";
    // 	$teks_1 = "to";
    // 	$teks_2 = "of";
    // 	$teks_3 = "records";
    // }
    $func_name = "pageLoad";
    if (isset($page_data['load_func_name']))
    {
        if ($page_data['load_func_name'])
            $func_name = $page_data['load_func_name'];
    }
    $limit = $page_data['limit'];
    $limit = $limit?$limit:1;
    $count = ceil($page_data['count_row'] / $limit) ;
    $last_row = $limit*$page_data['current'];
    if ($last_row > $page_data['count_row'])
        $last_row = $page_data['count_row'];
        $page_result = '<div class="d-flex justify-content-between align-items-center flex-wrap">';
            $page_result .= '<div class="d-flex flex-wrap py-2 mr-3">';
                $page_result .= '<a href="javascript:void(0);" '.($page_data['current']==1?'':'onclick="load_list(`'.$func_name.'`,`1`);"').' class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 '.($page_data['current']==1?'active':'').'"><i class="ki ki-bold-arrow-back icon-xs"></i></a>';
                $paging_show = 2;
                $page_start = $page_data['current'] - $paging_show;
                $page_start = $page_start<1?1:$page_start;
                //$page_end	= $count;
                $page_end = $page_data['current'] + $paging_show;
                $page_end = $count > $page_end ? $page_end : $count;
                $page_end = $count > 1 ? $page_end : 1;
                //
                if ($page_start > 1)
                {
                    $page_result .= '<a href="javascript:void(0);" onclick="'.$limit*($page_data['current']-1).'." class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">...</a>';
                }
                // before current
                for($i=$page_start; $i<=$page_end; $i++)
                {
                    $page_result .= '<a href="javascript:void(0);" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1  '.($page_data['current']==$i?'active':'').'" '.($page_data['current']==$i?'':'onclick="load_list(`'.$func_name.'`,`'.$i.'`);"').'>'.$i.'</a>';
                }
                // after current
                if ($page_end < $count)
                {
                    $page_result .= '<a href="javascript:void(0);" onclick="'.$limit*($page_data['current']-1).'." class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">...</a>';
                }
                $page_result .= '
                <a href="javascript:void(0);" onclick="load_list(`'.$func_name.'`,`'.$count.'`);" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 '.($page_data['current']==$page_end?'active':'').'"><i class="ki ki-bold-arrow-next icon-xs"></i></a>';
            $page_result .= '</div>';
            $page_result .= '<div class="d-flex align-items-center py-3">';
                $page_result .= '<span class="text-muted"> '.$teks.' '.(($limit*($page_data['current']-1))+1).' '.$teks_1.' '.$last_row.' '.$teks_2.' '.$page_data['count_row'].' '.$teks_3.'</span>';
            $page_result .= '</div>';
        $page_result .= '</div>';
        return $page_result;
}