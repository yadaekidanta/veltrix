<?php defined('BASEPATH') or exit('No direct script access allowed');
// set global $this
if (!function_exists('this')) {
	function this()
	{
		$CI = &get_instance();
		return $CI;
	}
}

if (!function_exists('array_group_by')) {
	/**
	 * Groups an array by a given key.
	 *
	 * Groups an array into arrays by a given key, or set of keys, shared between all array members.
	 *
	 * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
	 * This variant allows $key to be closures.
	 *
	 * @param array $array   The array to have grouping performed on.
	 * @param mixed $key,... The key to group or split by. Can be a _string_,
	 *                       an _integer_, a _float_, or a _callable_.
	 *
	 *                       If the key is a callback, it must return
	 *                       a valid key from the array.
	 *
	 *                       If the key is _NULL_, the iterated element is skipped.
	 *
	 *                       ```
	 *                       string|int callback ( mixed $item )
	 *                       ```
	 *
	 * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
	 */
	function array_group_by(array $array, $key)
	{
		if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
			trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
			return null;
		}

		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;

		// Load the new array, splitting by the target key
		$grouped = [];
		foreach ($array as $value) {
			$key = null;

			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && property_exists($value, $_key)) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}

			if ($key === null) {
				continue;
			}

			$grouped[$key][] = $value;
		}

		// Recursively build a nested grouping if more parameters are supplied
		// Each grouped array value is grouped according to the next sequential key
		if (func_num_args() > 2) {
			$args = func_get_args();

			foreach ($grouped as $key => $value) {
				$params = array_merge([$value], array_slice($args, 2, func_num_args()));
				$grouped[$key] = call_user_func_array('array_group_by', $params);
			}
		}

		return $grouped;
	}
}

function getNameFromNumber($num)
{
	$numeric = $num % 26;
	$letter = chr(65 + $numeric);
	$num2 = intval($num / 26);
	if ($num2 > 0) {
		return getNameFromNumber($num2 - 1) . $letter;
	} else {
		return $letter;
	}
}

if (!function_exists('guzzle')) {
	function guzzle()
	{
		$client = new Client([
			'base_uri' => $CI->config->item('base_url_api') . $CI->config->item('base_url_ci')
		]);
		return $client;
	}
}

function check_session($guzzle)
{
	if (!isset($_SESSION['auth']) || empty($_SESSION['auth'])) {
		return false;
	}
	$auth = json_decode(this()->app_encryption->decrypt($_SESSION['auth']), true);
	$token = $auth['token'];
	try {
		$response = $guzzle->request('POST', 'login/c_login/auth', ['headers' => ['Authorization' => $token]]);
		$result   = json_decode($response->getBody()->getContents(), true);
	} catch (ClientException $e) {
		$result['status'] = $e->getResponse()->getStatusCode();
	} catch (ServerException $e) {
		$result['status'] = $e->getResponse()->getStatusCode();
	}

	if ($result['status'] == 202) {
		$status = true;
	} elseif ($result['status'] == 401) {
		$status = false;
	} elseif ($result['status'] == 404) {
		$status = false;
	} else {
		$status = false;
	}

	return $status;
}

function login_page()
{
	this()->session->sess_destroy();
	$protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http") . '://';
	header('location:' . this()->config->item('base_url_ms') . 'login/home?redirect=' . current_url());
}

if (!function_exists('trace')) {
	function trace($data, $stop = true)
	{
		echo "<pre>";
		var_dump($data);
		echo "</pre>";

		if ($stop) {
			die();
		}
	}
}

function url_mapping()
{
	$folder  = "";
	$script  = ltrim(str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']), "/");
	$raw     = explode("/", $script);
	$count   = count($raw);
	$current = $_SERVER['REQUEST_URI'];
	$x_raw   = explode('/', $current);

	foreach ($x_raw as $r) {
		if ($r != "") {
			$folder .= $r . "/";
		}
	}

	$router = str_replace($script, "", $folder);
	$sd     = str_replace(this()->router->fetch_method() . "/", "", $router);

	//nama url untuk service, subservice, menu, submenu
	$prepare_url  		 = explode("/", $router);
	$count_url 			 = count($prepare_url);
	$active_service 	 = $raw[$count - 2];
	$active_subservice 	 = $prepare_url[$count_url - 4];
	$active_menu 		 = $prepare_url[$count_url - 3];
	$active_submenu 	 = $prepare_url[$count_url - 2];

	$scheme = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http") . "://";
	$host   = $_SERVER['HTTP_HOST'] . "/";
	$url    = $scheme . $host . $script . $sd;

	$data = array(
		'url' 					 		=> $url,
		'active_service' 		=> $active_service,
		'active_subservice' => $active_subservice,
		'active_menu' 		  => $active_menu,
		'active_submenu' 		=> $active_submenu,
	);

	return $data;
}

if (!function_exists('idr_format')) {
	function idr_format($num)
	{
		$num_arr = explode('.', $num);

		$str_dec = !empty($num_arr[1]) ? ',' . $num_arr[1] : '';

		$hasil_rupiah = number_format(floatval($num_arr[0]), 0, ',', '.');
		return 'Rp. ' . $hasil_rupiah . $str_dec;
	}
}

if (!function_exists('idr_format2')) {
	function idr_format2($num)
	{
		$hasil_rupiah = "Rp " . $num;
		return $hasil_rupiah;
	}
}

if (!function_exists('idr')) {
	function idr($num)
	{
		$hasil_rupiah = "Rp " . number_format($num, 0, ',', '.');
		return $hasil_rupiah;
	}
}

function format_angka($num)
{
	$cek = explode(',', $num);
	if (!strpos($num, ',')) {
		$hasil = number_format($num, 0, ',', '.');
	} else {
		$hasil = $num;
	}
	return $hasil;
}

function hitung_umur($datetime, $datetime_now, $full = false)
{
	$now = new DateTime($datetime_now);
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'thn',
		'm' => 'bln',
		'w' => 'mgu',
		'd' => 'hri',
		'h' => 'jam',
		'i' => 'mnt',
		's' => 'dtk',
	);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) : '0';
}

/***************************
 *
 * Deskripsi
 * Mengambil properti user pada session auth
 *
 * Parameter
 * $type => nama property yang akan diambil (null = semua properti)
 *
 * Properti session auth yang disimpan pada saat user login
 * user_id
 * user_name
 * rs_name
 * rs_address
 * rs_area
 *
 **************************/

function getsessionValue($type = '')
{
	if (!isset($_SESSION['auth']) || empty($_SESSION['auth'])) {
		return false;
	}

	if ($type !== '') {
		$auth = json_decode(this()->app_encryption->decrypt($_SESSION['auth']), true);
		$auth = $auth[$type];
	} else {
		$auth = json_decode(this()->app_encryption->decrypt($_SESSION['auth']), true);
	}

	return $auth;
}

function periode($date = null)
{
	if (!$date) return 'Jan 1999';
	$strBulan = "Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember";
	$arrBulan = explode(' ', $strBulan);

	$month = date('m', strtotime($date));
	$year = date('Y', strtotime($date));

	return $arrBulan[(int) $month - 1] . " - " . $year;
}

function penyebut($nilai)
{
	$nilai = abs($nilai);
	$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " " . $huruf[$nilai];
	} else if ($nilai < 20) {
		$temp = penyebut($nilai - 10) . " belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
	}
	return $temp;
}

function terbilang($nilai)
{
	if ($nilai < 0) {
		$hasil = "minus " . trim(penyebut($nilai));
	} else {
		$hasil = trim(penyebut($nilai));
	}
	return $hasil;
}

function pembulatan($nilai)
{
	$puluhan = substr($nilai, -2);
	return 100 - $puluhan;
}

function valid_date($date)
{
	$d = DateTime::createFromFormat('Y-m-d', $date);
	return $d && $d->format('Y-m-d') === $date;
}

if (!function_exists('check_access')) {
	function check_access($id_dept)
	{
		$CI = &get_instance();
		// if ($CI->data_session['role'] == 'Z') {
		// 	$cek = true;
		// } else {
		$list_dept = json_decode($CI->data_session['list_departements'], true);
		$cek = false;

		if ($list_dept) {
			foreach ($list_dept as $k => $v) {
				if ($v['id_dept'] == $id_dept) {
					$cek = true;
				}
			}
		}
		// }

		return $cek;
	}
}

function check_already_login()
{
    $ci = &get_instance();
    $user_session = $ci->session->userdata('user_login');
    if ($user_session) {
        header("Location: " . base_url('home'));
        exit();
    }
}

function check_not_login()
{
    $ci = &get_instance();
    $user_session = $ci->session->userdata('user_login');
    if (!$user_session) {
        header("Location: " . base_url('login'));
        exit();
    }
}