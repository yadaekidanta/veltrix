<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function json_result_200($param){
    return response(['code'=>200,'data'=>$param,'error'=>null],200);
}

function json_result_401(){
    return response(['code'=>401,'data'=>null,'error'=>'Unauthorized.'],401);
}

function json_result_403(){
    return response(['code'=>403,'data'=>null,'error'=>'URL not Found.'],403);
}

function json_result_422($param){
    return response(['code'=>422,'data'=>null,'error'=>$param],422);
}
function json_result_411($param){
    return response(['code'=>411,'data'=>null,'error'=>$param],411);
}

function json_result_500($param = null){
    if($param==null){
        $param = 'Internal Server Error';
    }
    // return 'test';
    // print_r($param);exit;
    return response(['code'=>500,'data'=>null,'error'=>$param],500);
}

function response($data,$code)
{
    header('Content-Type: application/json'); 
    http_response_code($code);
	echo json_encode($data,true);
}