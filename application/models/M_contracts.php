<?php

class M_contracts extends Base_model {
    function __construct()
    {
        parent::__construct();
        $this->set_table('cp_contracts');
    }
    function count_list()
	{
        $this->db->select('count(tbl.id) as num_rows');
		if ($this->where)
		{
			if (count($this->like)>0)
			{
				$like = '( 1=0 ';
				foreach ($this->like as $key => $value)
				{
					$like .= ' OR '.$key." LIKE '%".$value."%'";
				}
				$like .= ')';
				$this->where[$like] = null;
			}
			$this->db->where($this->where);
		}else
		{
			$this->db->or_like($this->like);
		}

		$query = $this->db->get($this->table.' tbl');
		$data = $query->row_array();
		return $data['num_rows'];
	}

    function list()
    {
        $this->db->select('tbl.*');
        $this->db->where($this->where);

        foreach ($this->order_by as $key => $value) {
            $this->db->order_by($key, $value);
        }

        if (!$this->limit and !$this->offset)
            $query = $this->db->get($this->table . ' tbl');
        else
            $query = $this->db->get($this->table . ' tbl', $this->limit, $this->offset);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }
    function get_new_code()
	{
		$preff = 'C/'.date('y').'/';
		$preffLen = strlen($preff)+1;
		$value = '';
		$this->db->select('contract_number');
		$this->db->order_by('contract_number','desc');
		$where = array();
		$where['substr("contract_number",0,'.$preffLen.')'] = $preff;
		$this->db->where($where);
		$query = $this->db->get($this->table.' tbl',1);
		$row = $query->row_array();
		// echo $this->db->last_query(); exit;
		//
		$counter = 0;
		if ($row['contract_number']){
			$counter = substr($row['contract_number'],$preffLen);
		}
		$counter = substr($counter + 1,1);
		$value = $preff.'-'.$counter;
		return $value;
	}
    // function get_new_code()
    // {

    //   $this->db->select('SUBSTRING_INDEX(SUBSTRING_INDEX(contract_number,\'/\',3),\'/\',-1) as last_contract_code ');
    //   $this->db->order_by('contract_number', 'DESC');
    //   $query = $this->db->get($this->table.' tbl');
    //   $data = $query->row_array();
    //   $last_contract = $data['last_contract_code']+1;
    //   return 'C/'.date('y').'/'.$last_contract;
    // }
}
