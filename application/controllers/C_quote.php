<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_quote extends CI_Controller {
    public $data = array('content' => 'Maaf, Data Tidak Ditemukan');
    function __construct()
	{
		parent::__construct();
		$this->load->model('M_contracts');
		$this->load->model('M_contracts_details');
		$this->load->model('M_contracts_package');
		$this->load->model('M_contracts_payment_plans');
		$this->load->model('M_master_quote_tarif');
		$this->load->model('M_master_quote_tnc');
		$this->load->model('M_user');
	}
    public function index()
    {
        $data = array();
        $data['tnc'] = $this->M_master_quote_tnc->list();
        $group = array();
        $group['`group`'] = null;
		    $this->M_master_quote_tarif->set_group($group);
        $data['group'] = $this->M_master_quote_tarif->get_group();
        // var_dump($data['tarif']->result_array());exit;
        

        $data['content'] = 'quote/main';
        $this->load->view($data['content'], $data);
    }
    public function list($pg=1)
    {
        $limit = 10;
		// filter
		$filter = array();
		$filter['filter'] = $this->input->post('f_search');
		// condition
		$where = array();
		if ($filter['filter']){
			$where['(
				tbl.name LIKE \'%'.$filter['filter'].'%\'
			)'] = null;
        }
        $this->M_contracts->set_where($where);
        $order = array();
        $order['id'] = "ASC";
		    $this->M_contracts->set_order($order);
        $this->M_contracts->set_limit($limit);
        $this->M_contracts->set_offset($limit * ($pg - 1));
        $paging = array();
		//
		$paging = array();
		$paging['limit'] 		= $limit;
		$paging['count_row'] 	= $this->M_contracts->count_list() ;
		$paging['current'] 		= $pg;
		$paging['load_func_name'] = "C_quote/list";
		$paging['list'] 		= gen_paging($paging);
		$list = $this->M_contracts->list();
		//
		$data = array();
		$data['paging'] = $paging;
		$data['content'] = 'list';
		$data['list'] 	= $list;
		$data['filter'] = $filter;
		$data['key'] = $filter;
		$data['count_row'] 	= $this->M_contracts->count_list();
        $data['content'] = 'quote/list';
        $this->load->view($data['content'], $data);
    }
    public function input()
    {
        $data['content'] = 'quote/input';
        $this->load->view($data['content'], $data);
    }

	public function save()
	{
    $form_data =  json_encode($_POST);
    // echo $form_data;exit;
    $form_data =  json_decode($form_data);
		$user = array();
		$user = array();
    $contract_terms = array();
    $this->db->trans_start();
		// list($name1, $name2) = $name;
		// print_r($name2);exit;
    for ($i=0; $i < count($form_data->email); $i++) {
      $a = $i+1;
      $user_email = $this->M_user->get(array('email'=>$form_data->email[$i]));
      $user_cell_number = $this->M_user->get(array('cell_number'=>$form_data->cell_number[$i]));
      if ($user_email['email']) {
        return json_result_422('Email duplicate');
      }
      if ($user_cell_number['cell_number']) {
        return json_result_422('Cell number duplicate');
      }

      $user['id'] = 0;
      $user['name'] = $form_data->name[$i];
      $user['email'] = $form_data->email[$i];
      $user['cell_number'] = $form_data->cell_number[$i];
      $user['role'] = 'CLIENT';
      // $this->M_user->save($user);
      $this->db->insert('cp_users',$user);
      ${'id_user' . $a} =  $this->db->insert_id();
      ${'name_user' . $a} =  $user['name'];

      array_push($contract_terms,array(
        'title' => $form_data->title_master_contract_terms[$i],
        'contract_terms' => $form_data->contract_terms[$i],
      ));

    }

    $contract = array();
    $code = $this->M_contracts->get_new_code();
    $contract['id'] = 0;
    $contract['contract_number'] = $code;
    $contract['event_date'] = date('Y-m-d');
    $contract['id_users1'] = $id_user1;
    $contract['id_users2'] = $id_user2;
    $contract['nama1'] = $name_user1;
    $contract['nama2'] = $name_user2;
    $contract['contract_terms'] = json_encode($contract_terms);

    // $this->M_contracts->save($contract);
    $this->db->insert('cp_contracts',$contract);
    $id_contract =  $this->db->insert_id();

    $contract_detail = array();

    for ($i=0; $i < count($form_data->nama_tarif); $i++) {
      if ($form_data->nama_tarif[$i]) {
        // $get_data_tarif =  $this->->get($form_data->$id_tarif[$i]);
        $contract_detail['id'] = 0;
        $contract_detail['id_contracts'] = $id_contract;
        $contract_detail['id_master_tarif'] = $form_data->id_tarif[$i];
        $contract_detail['nama_tarif'] = $form_data->nama_tarif[$i];
        $contract_detail['group'] = $form_data->group[$i];
        $contract_detail['harga'] = $form_data->harga_tarif[$i];

        $this->db->insert('cp_contracts_details',$contract_detail);
        // $this->M_contracts_details->save($contract_detail);
      }
    }

    $contract_plans = array();
    for ($i=0; $i < count($form_data->due_date); $i++) {
      switch ($i+1) {
        case 1:
          $alias = 'ST';
        break;
        case 2:
          $alias = 'ND';
        break;
        case 3:
          $alias = 'RD';
        break;
        default:
          $alias = 'TH';
        break;
      }
      $contract_plans['id'] = 0;
      $contract_plans['id_contracts'] = $id_contract;
      $contract_plans['contract_number'] = $contract['contract_number'];
      $contract_plans['amount'] = $form_data->amount[$i];
      $contract_plans['due_date'] = $form_data->due_date[$i];
      $contract_plans['payment_sequence'] = $i.$alias.' PAYMENT';
      $contract_plans['desc'] = $form_data->desc[$i];

      // $this->M_contracts_payment_plans->save($contract_plans);
      $this->db->insert('cp_contracts_payment_plans',$contract_plans);
    }
    $this->db->trans_complete();

    return json_result_200(str_replace('=','',base64_encode($id_contract)));
    // if ($this->db->trans_complete()) {
    //   return json_result_200('Save quote berhasil');
    // }else{
    //   return json_result_422('Save quote gagal');
    // }

	}

  function pdf($id_contract)
  {
     $id_contract = base64_decode($id_contract);
     $this->load->library('pdf');

     $data = array();
     // $data['content'] = 'quote/pdf';
     $data['contract'] = $this->M_contracts->get(array('id'=>$id_contract));
     $data['user1'] = $this->M_user->get(array('id'=>$data['contract']['id_users1']));
     $data['user2'] = $this->M_user->get(array('id'=>$data['contract']['id_users2']));

     $this->M_contracts_details->set_where(array('id_contracts'=>$id_contract));
     $data['contract_detail'] = $this->M_contracts_details->get_list();
     $this->M_contracts_payment_plans->set_where(array('id_contracts'=>$id_contract));
     $data['contracts_payment'] = $this->M_contracts_payment_plans->get_list();

     // $this->load->view($data['content'], $data);
     $html = $this->load->view('quote/pdf', $data, true);
     $this->pdf->createPDF($html, 'mypdf', false);
  }
}
