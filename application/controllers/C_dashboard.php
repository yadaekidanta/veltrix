<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class C_dashboard extends CI_Controller {
    public $data = array('content' => 'Maaf, Data Tidak Ditemukan');
    public function index()
    {
        $data['content'] = 'dashboard/main';
        $this->load->view($data['content'], $data);
    }
}
